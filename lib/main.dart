import 'dart:developer';

import 'package:block_practice_1/block.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: BlocProvider(
        create: (_) => CounterBlock(),
        child: Builder(
          builder: (ctx) => Scaffold(
            appBar: AppBar(
              title: Text('Material App Bar'),
            ),
            body: Center(
              child: StreamBuilder<CounterState>(
                stream: ctx.read<CounterBlock>(),
                builder: (ctx, snapshot){
                  print('Build');
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(snapshot.data?.counter.toString()),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () => ctx.read<CounterBlock>().add(DecrementCounterEvent()),
                            icon: Icon(Icons.remove),
                          ),
                          IconButton(
                            onPressed: () => ctx.read<CounterBlock>().add(ResetCounterEvent()),
                            icon: Icon(Icons.replay_outlined),
                          ),
                          IconButton(
                            onPressed: () => ctx.read<CounterBlock>().add(IncrementCounterEvent()),
                            icon: Icon(Icons.add),
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
