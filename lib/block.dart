import 'package:flutter_bloc/flutter_bloc.dart';

abstract class CounterEvent {}

class ResetCounterEvent extends CounterEvent {}
class IncrementCounterEvent extends CounterEvent {}
class DecrementCounterEvent extends CounterEvent {}

class CounterState {
  final int counter;

  CounterState({this.counter});

  CounterState copyWith(int counter) {
    return CounterState(counter: counter);
  }
}

class CounterBlock extends Bloc<CounterEvent, CounterState> {
  CounterBlock() : super(CounterState(counter: 0));

  @override
  Stream<CounterState> mapEventToState(CounterEvent event) async* {
    print('${event.runtimeType} in [CounterBlock]');
    switch (event.runtimeType) {
      case IncrementCounterEvent:
        yield state.copyWith(state.counter + 1);
        break;
      case DecrementCounterEvent:
        yield state.copyWith(state.counter - 1);
        break;
      case ResetCounterEvent:
        yield state.copyWith(0);
        break;
    }
  }
}
